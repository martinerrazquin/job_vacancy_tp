Feature: search job offers by title and location

  Background:
    Given It exists a Job offer with title "Chef Job" and location "London"
    And It exists a Job offer with title "Chef Job" and location "Madrid"
    And I access the offers list page

  Scenario: Search a word in a title with two results
    When I fill the title field with "Chef"
    And I search it
    Then I should see the London chef job offer in Current Job Offers
    And I should see the Madrid chef job offer in Current Job Offers

  Scenario: Search a word in a location with one result
    When I fill the location field with "London"
    And I search it
    Then I should see the London chef job offer in Current Job Offers
    And I should not see the Madrid chef job offer in Current Job Offers

  Scenario: Search a word in a title with no results
    When I fill the title field with "Programmer"
    And I search it
    Then I should not see any job offer in Current Job Offers

  Scenario: Search a word in a location with no results
    When I fill the location field with "Rio"
    And I search it
    Then I should not see any job offer in Current Job Offers

  Scenario: Search a word in a title and a word in a location with one result
    When I fill the title field with "Chef"
    And I fill the location field with "London"
    And I search it
    Then I should see the London chef job offer in Current Job Offers
    And I should not see the Madrid chef job offer in Current Job Offers

  Scenario: Search a non existing word in a title and an existing word in a location
    When I fill the title field with "Programmer"
    And I fill the location field with "London"
    And I search it
    Then I should not see any job offer in Current Job Offers

  Scenario: Search an existing word in a title and a non existing word in a location
    When I fill the title field with "Chef"
    And I fill the location field with "Rio"
    And I search it
    Then I should not see any job offer in Current Job Offers

  Scenario: Search a word in mayus in a location with one result
    When I fill the location field with "LONdon"
    And I search it
    Then I should see the London chef job offer in Current Job Offers
    And I should not see the Madrid chef job offer in Current Job Offers
