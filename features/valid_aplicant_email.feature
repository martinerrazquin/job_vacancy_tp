Feature: Applicant email validation

  Background:
    Given I am applying for a job offer

  Scenario: Applying with a valid global email
    When I fill the email field with "valid_.-email123@domain.org"
    And I submit the application
    Then I should see "Contact information sent."

  Scenario: Applying with a valid local email
    When I fill the email field with "valid.email@domain.org.ar"
    And I submit the application
    Then I should see "Contact information sent."

  Scenario: Applying with an invalid email
    When I fill the email field with "forgot-.org@domain"
    And I submit the application
    Then I should see "Invalid email address."

  Scenario: Applying with a blank email
    When I leave the email field blank
    And I submit the application
    Then I should see "Invalid email address."

  Scenario: Applying with an invalid email that not start whit a letter
    When I fill the email field with "-foo@bar.com"
    And I submit the application
    Then I should see "Invalid email address."

  Scenario: Applying with an invalid email and expected pay
    When I fill the email field with "forgot-.org@domain"
    And I fill the expected pay field with -10
    And I submit the application
    Then I should see "Invalid email address."
    And I should see "Invalid expected pay: please enter a value between $0 and $150.000"