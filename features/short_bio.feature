Feature: Include a short bio in job application


  Scenario: Apply to a job application with a blank short bio
    Given I am applying for a job offer
    When I fill the email field with "applicant@test.com"
    And I leave the short bio field blank
    And I submit the application
    Then I should receive a mail with offerer info
    And it should say that the short bio was not completed


  Scenario: Apply to a job application with a valid short bio
    Given I am applying for a job offer
    When I fill the email field with "applicant@test.com"
    And I fill the short bio field with "I am groot"
    And I submit the application
    Then I should receive a mail with offerer info
    And it should include "I am groot" as the short bio
