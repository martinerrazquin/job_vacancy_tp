Given(/^There are no applications for the offer$/) do
end

When(/^I click on the applications list$/) do
  visit '/job_offers/my'
  click_link 'View Applications'
end

Then(/^I should see that there are no applications yet$/) do
  page.should have_content('There are no applications for this offer.')
end

Given(/^User "(.*?)" applied to my offer$/) do |email|
  visit '/job_offers'
  click_link 'Apply'
  fill_in('job_application[applicant_email]', :with => email)
  click_button('Apply')
end

Then(/^i should see the user's "(.*?)" application info$/) do |email|
  times_found = 0
  all("tr").each do |tr|
    if tr.has_content?(email)
      times_found += 1
    end
  end
  expect(times_found).to eq 1
end
