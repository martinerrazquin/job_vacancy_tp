Given(/^a user "(.*?)" with password "(.*?)" exists$/) do |email, password|
  visit '/register'
  fill_in('user[name]', :with => 'name')
  fill_in('user[email]', :with => email)
  fill_in('user[password]', :with => password)
  fill_in('user[password_confirmation]', :with => password)
  click_button('Create')
end

When(/^I try to log in as "(.*?)" with password "(.*?)"$/) do |email, password|
  visit '/login'
  fill_in('user[email]', :with => email)
  fill_in('user[password]', :with => password)
  click_button('Login')
end

Then(/^I should see an error informing my account will be blocked if I fail authentication one more time$/) do
  page.should have_content('For security reasons, your account will be blocked after the next failed attempt.')
end

Then(/^I should see an error informing my account has been blocked$/) do
  page.should have_content('For security reasons,
        your account has been blocked. Please check your email for steps to unblock it.')
end

Then(/^I should receive an email with steps to unlock$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/forgetful_user@gmail.com", "r")
  content = file.read
  content.empty?.should be false
  content.include?('/restore_password').should be true
end

Given(/^"(.*?)" account has been blocked$/) do |email|
  visit '/login'
  3.times do
    fill_in('user[email]', :with => email)
    fill_in('user[password]', :with => 'x')
    click_button('Login')
  end
end

When(/^I click the link provided to unlock my account$/) do
  visit '/restore_password'
end

When(/^I enter the email "(.*?)" of the bolcked account$/) do |email|
  fill_in('user[email]', :with => email)
end

When(/^I enter a new password "(.*?)"$/) do |password|
  fill_in('user[password]', :with => password)
end

When(/^I validate my new password as "(.*?)"$/) do |password|
  fill_in('user[password_confirmation]', :with => password)
end

When(/^I submit$/) do
  click_button('Restore')
end

Then(/^I should be redirected to the login screen$/) do
#  visit '/login'
  page.should have_content('Login')
end

Then(/^I should be able to login as "(.*?)" with password "(.*?)"$/) do |email, password|
#  visit '/login'
  fill_in('user[email]', :with => email)
  fill_in('user[password]', :with => password)
  click_button('Login')
  page.should have_content(email)
end

Then(/^I should see an error informing me that passwords do not match$/) do
  page.should have_content('Passwords do not match')
end

Then(/^I should see an error informing me that that account in not blocked$/) do
  page.should have_content('This account is not blocked')
end

Then(/^I should see an error informing me that the account has been blocked$/) do
  page.should have_content('For security reasons,
        your account has been blocked. Please check your email for steps to unblock it.')
end
