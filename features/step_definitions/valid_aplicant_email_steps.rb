When(/^I fill the email field with "(.*?)"$/) do |email|
  fill_in('job_application[applicant_email]', :with => email)
end

When(/^I leave the email field blank$/) do
end
