Given(/^I am applying for a job offer$/) do
  steps %Q{
    Given only a "Web Programmer" offer exists in the offers list
    And I access the offers list page
  }
  click_link 'Apply'
end

When(/^I fill the expected pay field with (\-?\d+)$/) do |value|
  fill_in('job_application[expected_pay]', :with => value)
end

When(/^I submit the application$/) do
  click_button('Apply')
end

When(/^I leave the expected pay field blank$/) do
end

Then(/^I should see the (\d+) expected pay in the email sent$/) do |expected_pay|
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/valid_email@domain.com", "r")
  content = file.read
  content.include?(expected_pay).should be true
end

Then(/^I should see that the expected pay was not informed in the email sent$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/valid_email@domain.com", "r")
  content = file.read
  content.include?("not informed").should be true
end
