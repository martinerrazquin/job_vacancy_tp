Given(/^It exists a Job offer with title "(.*?)" and location "(.*?)"$/) do |offer_title, offer_location|
  steps %Q{
    Given I am logged in as job offerer
  }
  visit '/job_offers/new'
  fill_in('job_offer[title]', :with => offer_title)
  fill_in('job_offer[location]', :with => offer_location)
  click_button('Create')
end

When(/^I fill the title field with "(.*?)"$/) do |search_title|
  fill_in('title_search', :with => search_title)
end

When(/^I search it$/) do
  click_button('search')
end

Then(/^I should see the London chef job offer in Current Job Offers$/) do
  page.should have_content('London')
end

Then(/^I should see the Madrid chef job offer in Current Job Offers$/) do
  page.should have_content('Madrid')
end

When(/^I fill the location field with "(.*?)"$/) do |search_location|
  fill_in('location_search', :with => search_location)
end

Then(/^I should not see the Madrid chef job offer in Current Job Offers$/) do
  page.should_not have_content('Madrid')
end

Then(/^I should not see any job offer in Current Job Offers$/) do
  page.should_not have_content('Madrid')
  page.should_not have_content('London')
end
