When(/^I leave the short bio field blank$/) do
end

Then(/^it should say that the short bio was not completed$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", "r")
  content = file.read
  content.include?("Short Bio: not informed").should be true
end

When(/^I fill the short bio field with "(.*?)"$/) do |short_bio_text|
  fill_in('job_application[short_bio]', :with => short_bio_text)
end

Then(/^it should include "(.*?)" as the short bio$/) do |expected_short_bio|
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", "r")
  content = file.read
  content.include?(expected_short_bio).should be true
end
