When(/^I mark it as satisfied$/) do
  check('job_offer[is_satisfied]')
end

When(/^I mark it as unsatisfied$/) do
  uncheck('job_offer[is_satisfied]')
end

Then(/^I should see "(.*?)" in Job Offers$/) do |content|
  visit '/job_offers'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in Job Offers$/) do |content|
  visit '/job_offers'
  page.should_not have_content(content)
end