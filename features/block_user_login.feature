Feature: Block account after repeated successive failed login attempts

  Background:
    Given a user "forgetful_user@gmail.com" with password "mypassword" exists

  Scenario: Warning after 2 failed login attempts
    When I try to log in as "forgetful_user@gmail.com" with password "notmypassword"
    And I try to log in as "forgetful_user@gmail.com" with password "notmypassword"
    Then I should see an error informing my account will be blocked if I fail authentication one more time

  Scenario: Block account after 3 successive failed login attempts
    When I try to log in as "forgetful_user@gmail.com" with password "notmypassword"
    And I try to log in as "forgetful_user@gmail.com" with password "notmypassword"
    And I try to log in as "forgetful_user@gmail.com" with password "notmypassword"
    Then I should see an error informing my account has been blocked
    And I should receive an email with steps to unlock

  @wip
  Scenario: successfully unlock a blocked account
    Given "forgetful_user@gmail.com" account has been blocked
    When I click the link provided to unlock my account
    And I enter a new password "newpassword"
    And I validate my new password as "newpassword"
    And I submit
    Then I should be redirected to the login screen
    And I should be able to login as "forgetful_user@gmail.com" with password "newpassword"

  Scenario: try to unlock a blocked account but password and confirmation do not match
    Given "forgetful_user@gmail.com" account has been blocked
    When I click the link provided to unlock my account
    And I enter the email "forgetful_user@gmail.com" of the bolcked account
    And I enter a new password "newpassword"
    And I validate my new password as "notmynewpassword"
    And I submit
    Then I should see an error informing me that passwords do not match

  @wip
  Scenario: try to unlock a non-blocked account
    Given I click the link provided to unlock my account
    And I enter the email "forgetful_user@gmail.com" of the bolcked account
    And I enter a new password "password"
    And I validate my new password as "password"
    And I submit
    Then I should see an error informing me that that account in not blocked

  Scenario: try to login when account is blocked
    Given "forgetful_user@gmail.com" account has been blocked
    When I try to log in as "forgetful_user@gmail.com" with password "mypassword"
    Then I should see an error informing me that the account has been blocked