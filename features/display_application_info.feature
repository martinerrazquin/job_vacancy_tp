Feature: Save Application Info for a Job Offer and display it to Job Offer owner

  Background:
    Given I am logged in as job offerer
    And I have "Programmer vacancy" offer in My Offers

  Scenario: Display applications when there are none
    Given There are no applications for the offer
    When I click on the applications list
    Then I should see that there are no applications yet

  Scenario: Display applications when there are two
    Given User "applier1@yahoo.com.ar" applied to my offer
    And User "applier2@gmail.com" applied to my offer
    When I click on the applications list
    Then i should see the user's "applier1@yahoo.com.ar" application info
    And i should see the user's "applier2@gmail.com" application info