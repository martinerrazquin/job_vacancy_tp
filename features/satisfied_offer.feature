Feature: Mark an offer as satisfied

  Background:
    Given I am logged in as job offerer
    And I have "Carpenter Job" offer in My Offers
    And I edit it

  Scenario: Mark an active offer as satisfied
    When I mark it as satisfied
    And I save the modification
    Then I should not see "Carpenter Job" in Job Offers

  Scenario: Mark an active offer as unsatisfied
    When I mark it as unsatisfied
    And I save the modification
    Then I should see "Carpenter Job" in Job Offers