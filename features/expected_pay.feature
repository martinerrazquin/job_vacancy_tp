Feature: Add expected pay to job application

  Background:
    Given I am applying for a job offer
    And I fill the email field with "valid_email@domain.com"

  Scenario: Apply with a 10000 expected pay
    When I fill the expected pay field with 10000
    And I submit the application
    Then I should see "Contact information sent."
    And I should see the 10000 expected pay in the email sent

  Scenario: Apply with a negative expected pay
    When I fill the expected pay field with -10
    And I submit the application
    Then I should see "Invalid expected pay: please enter a value between $0 and $150.000"

  Scenario: Apply with a 200000 expected pay
    When I fill the expected pay field with 200000
    And I submit the application
    Then I should see "Invalid expected pay: please enter a value between $0 and $150.000"

  Scenario: Apply without specifying expected pay
    When I leave the expected pay field blank
    And I submit the application
    Then I should see "Contact information sent."
    And I should see that the expected pay was not informed in the email sent
