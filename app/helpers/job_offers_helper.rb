# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  def current_offer
    @current_offer ||= JobOffer.find_by_id(params[:offer_id])
  end
end
