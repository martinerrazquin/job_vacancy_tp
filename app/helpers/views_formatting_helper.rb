JobVacancy::App.helpers do
  
  def to_yes_no(bool)
    bool ? 'Yes' : 'No'
  end

end
