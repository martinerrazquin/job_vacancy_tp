class JobOffer
  include DataMapper::Resource

  property :id, Serial
  property :title, String
  property :location, String
  property :description, String
  property :created_on, Date
  property :updated_on, Date
  property :is_active, Boolean, :default => true
  property :is_satisfied, Boolean, :default => false
  belongs_to :user
  has n, :job_applications, :constraint => :destroy

  validates_presence_of :title

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def self.all_active_not_satisfied
    JobOffer.all(:is_active => true, :is_satisfied => false)
  end

  def self.find_by_owner(user)
    JobOffer.all(:user => user)
  end

end
