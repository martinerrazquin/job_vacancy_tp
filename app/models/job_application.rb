class JobApplication
  include DataMapper::Resource

  LOWER_PAY_LIMIT = 0
  UPPER_PAY_LIMIT = 150000
  EMAIL_ERROR = 'Invalid email address.'
  EXPECTED_PAY_ERROR = 'Invalid expected pay: please enter a value between $0 and $150.000.'

  property :id, Serial
  property :applicant_email, String
  property :expected_pay, Integer
  property :short_bio, String, :length => 500
  belongs_to :job_offer

  validates_presence_of :applicant_email,
                        :message => EMAIL_ERROR
  validates_format_of :applicant_email,
                      :with => :email_address,
                      :message => EMAIL_ERROR
  validates_format_of :applicant_email,
                      :with => /\A[a-z]/i,
                      :message => EMAIL_ERROR
  validates_acceptance_of :expected_pay,
                          :accept => LOWER_PAY_LIMIT..UPPER_PAY_LIMIT,
                          :message => EXPECTED_PAY_ERROR

  def expected_pay=(expected_pay)
    expected_pay = nil if expected_pay == ''
    super
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end

end
