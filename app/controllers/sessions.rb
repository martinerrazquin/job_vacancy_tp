JobVacancy::App.controllers :sessions do

  BLOCK_WARNING_MESSAGE = 'For security reasons, your account will be blocked after the next failed attempt.'
  BLOCK_MESSAGE = 'For security reasons, your account has been blocked. Please check your email for steps to unblock it.'

  get :login, :map => '/login' do
    @user = User.new
    render 'sessions/new'
  end

  get :restore_password, :map => '/restore_password' do
    @user = User.new
    render 'sessions/restore_password'
  end

  post :new_password do
    email = params[:user][:email]
    password = params[:user][:password]
    flash.now[:error] = []

    unless User.exists?(email)
      @user = User.new
      flash.now[:error].push('This account is not blocked')
      render 'sessions/restore_password'
      return
    end

    user = User.get_user(email)
    if user.blocked?
      password_confirmation = params[:user][:password_confirmation]
      params[:user].reject!{|k,v| k == 'password_confirmation'}
      if params[:user][:password] == password_confirmation
        user.password = password
        if user.save
          flash[:success] = 'Password restored'
          redirect '/login'
        else
          flash.now[:error].push('All fields are mandatory')
          render 'sessions/restore_password'
        end
      else
        @user = User.new(params[:user])
        flash.now[:error].push('Passwords do not match')
        render 'sessions/restore_password'
      end
    else
      @user = User.new
      flash.now[:error].push('This account is not blocked')
      render 'sessions/restore_password'
    end
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]
    @user = User.authenticate(email, password)

    if User.exists?(email)
      user = User.get_user(email)
      if user.has_password?(password) and not user.blocked?
        user.reset_failed_login_attempts
        user.save
        sign_in @user
        redirect '/'
      else
        user.add_failed_login
        user.save
        @user = User.new
        flash.now[:error] = []
        flash.now[:error].push('Incorrect password')
        flash.now[:error].push(BLOCK_WARNING_MESSAGE) if user.show_block_warning?
        flash.now[:error].push(BLOCK_MESSAGE) if user.blocked?
        user.process if user.blocked?
        render 'sessions/new'
      end
    else
      @user = User.new
      flash.now[:error] = 'Invalid credentials'
      render 'sessions/new'
    end
  end

  get :destroy, :map => '/logout' do 
    sign_out
    redirect '/'
  end

end
