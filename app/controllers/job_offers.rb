JobVacancy::App.controllers :job_offers do

  get :my do
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :index do
    @offers = JobOffer.all_active_not_satisfied
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOffer.all_active_not_satisfied
    render 'job_offers/list'
  end

  get :edit, :with =>:offer_id  do
    @job_offer = current_offer
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, :with =>:offer_id  do
    @job_application = JobApplication.new
    @job_application.job_offer = current_offer
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/apply'
  end

  get :applications, :with =>:offer_id  do
    @job_offer = current_offer
    render 'job_offers/applications'
  end

  post :search do
    title_search = params[:title_search]
    if params[:title_search].present?
      @last_title_search = params[:title_search]
      title_search = params[:title_search].downcase
    end

    location_search = params[:location_search]
    if params[:location_search].present?
      @last_location_search = params[:location_search]
      location_search = params[:location_search].downcase
    end

    @offers = JobOffer.all(:conditions => ['lower(title) LIKE ? AND lower(location) LIKE ?',
                                          "%#{title_search}%",
                                          "%#{location_search}%"])
    render 'job_offers/list'
  end

  post :apply, :with => :offer_id do
    @job_application = JobApplication.new(params[:job_application])
    @job_application.job_offer = current_offer

    if @job_application.save
      @job_application.process
      flash[:success] = 'Contact information sent.'
      redirect '/job_offers'
    else
      flash.now[:error] = @job_application.errors.full_messages.flatten
      render 'job_offers/apply'
    end
  end

  post :create do
    @job_offer = JobOffer.new(params[:job_offer])
    @job_offer.owner = current_user
    if @job_offer.save
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/new'
    end
  end

  post :update, :with => :offer_id do
    @job_offer = current_offer
    @job_offer.update(params[:job_offer])
    if @job_offer.save
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/edit'
    end
  end

  put :activate, :with => :offer_id do
    @job_offer = current_offer
    @job_offer.activate
    if @job_offer.save
      flash[:success] = 'Offer activated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/my'
    end  
  end

  delete :destroy do
    @job_offer = current_offer
    if @job_offer.destroy
      flash[:success] = 'Offer deleted'
    else
      flash.now[:error] = 'Title is mandatory'
    end
    redirect 'job_offers/my'
  end

end
