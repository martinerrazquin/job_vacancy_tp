# Job Vacancy Application (Team Río)

## Team members

* Martín Bosch
* Rodrigo Zapico
* Martín Errázquin


## How to start working

Once you have cloned this repository, follow these steps to start working:

* Run **_bundle install --without staging production_**, to install all application dependencies
* Run **_bundle exec rake_**, to run all tests and ensure everything is properly setup
* Run **PADRINO_ENV=development _bundle exec rake db:migrate_**, to setup the development database
* Run **PADRINO_ENV=development _bundle exec padrino start_ -h 0.0.0.0**, to start the application in development mode.

Once the application is running you can browse it on http://localhost:3000


### Start working with Postgres
---------------------

Perform the following commands to create the databases:
````
sudo su
su - postgres
psql
create database jobvacancy_development;
create database jobvacancy_test;
CREATE ROLE jobvacancy WITH LOGIN PASSWORD 'jobvacancy'  CREATEDB ;
GRANT ALL PRIVILEGES ON DATABASE "jobvacancy_development" to jobvacancy;
GRANT ALL PRIVILEGES ON DATABASE "jobvacancy_test" to jobvacancy;
````


## Conventions

* Follow existing coding conventions
* Use feature branch
* Add descriptive commits messages in English to every commit
* Write code and comments in English
* Use REST routes
