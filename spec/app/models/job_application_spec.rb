require 'spec_helper'

describe JobApplication do

  describe 'model' do
    subject { @job_application = JobApplication.new }

    it { should respond_to( :applicant_email ) }
    it { should respond_to( :expected_pay) }
    it { should respond_to( :job_offer) }
    it { should respond_to( :short_bio) }
  end

  DEFAULT_EMAIL = 'applicant@test.com'

  def new_job_application()
    job_application = JobApplication.new
    job_application.applicant_email = DEFAULT_EMAIL
    job_application.job_offer = JobOffer.new({:id => 1})
    job_application
  end

  describe 'process' do

    let(:job_application) { new_job_application }

    it 'should deliver contact info notification' do
      JobVacancy::App.should_receive(:deliver).with(:notification, :contact_info_email, job_application)
      job_application.process
    end

  end

  describe 'valid?' do

    let(:job_application) { new_job_application }

    it 'should be valid when expected pay is 10000' do
      job_application.expected_pay = 10000
      expect(job_application.valid?).to eq true
    end

    it 'should be invalid when expected pay is 150001' do
      job_application.expected_pay = 150001
      expect(job_application.valid?).to eq false
    end

    it 'should be invalid when expected pay is -1' do
      job_application.expected_pay = -1
      expect(job_application.valid?).to eq false
    end

    it 'should be valid when expected pay is left blank' do
      expect(job_application.valid?).to eq true
    end

    it 'should be valid when expected pay is an empty string' do
      job_application.expected_pay = ''
      expect(job_application.valid?).to eq true
    end

    it 'should be invalid when expected pay is not a number' do
      job_application.expected_pay = 'invalid'
      expect(job_application.valid?).to eq false
    end

    it 'should be invalid when email not end with .something' do
      job_application.applicant_email = 'forgot-.org@domain'
      expect(job_application.valid?).to eq false
    end

    it 'should be invalid when email is not entered' do
      job_application.applicant_email = ''
      expect(job_application.valid?).to eq false
    end

    it 'should be invalid when email not start with a letter' do
      job_application.applicant_email = '-foo@bar.com'
      expect(job_application.valid?).to eq false
    end

  end

end
