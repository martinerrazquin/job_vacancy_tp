require 'spec_helper'

describe JobOffer do

  describe 'model' do

    subject { @job_offer = JobOffer.new }

    it { should respond_to( :id) }
    it { should respond_to( :title ) }
    it { should respond_to( :location) }
    it { should respond_to( :description ) }
    it { should respond_to( :owner ) }
    it { should respond_to( :owner= ) }
    it { should respond_to( :created_on) }
    it { should respond_to( :updated_on ) }
    it { should respond_to( :is_active) }
    it { expect(subject.is_active).to eq(true) }
    it { should respond_to( :is_satisfied) }
    it { expect(subject.is_satisfied).to eq(false) }
    it { should respond_to( :job_applications) }

  end

  describe 'valid?' do

    let(:job_offer) do
      user = User.new
      user.id = 1
      job_offer = JobOffer.new
      job_offer.owner = user
      job_offer
    end

    it 'should be false when title is blank' do
      expect(job_offer.valid?).to eq false
    end

    it 'should be true when title is not blank' do
      job_offer.title = 'the title'
      expect(job_offer.valid?).to eq true
    end

  end

end
