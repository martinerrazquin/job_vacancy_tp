migration 8, :create_job_applications do
  up do
    create_table :job_applications do
      column :id, Integer, :serial => true
      column :applicant_email, String, :length => 255
      column :expected_pay, Integer
      column :job_offer_id, Integer
    end
  end

  down do
    drop_table :job_applications
  end
end
