migration 9, :add_short_bio_field_to_job_applications do
  up do
    modify_table :job_applications do
      add_column :short_bio, String, :length => 500
    end
  end

  down do
    modify_table :job_applications do
      drop_column :short_bio
    end
  end
end
