migration 7, :add_is_satisfied_field_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :is_satisfied, TrueClass, default: false
    end
  end

  down do
    modify_table :job_offers do
      drop_column :is_satisfied
    end
  end
end
